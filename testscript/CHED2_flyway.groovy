@Library('mylib2') _


def env_server = [
]

pipeline {

    options {
        disableConcurrentBuilds()
        timestamps()
    }
    parameters {
        choice(name: 'Environments',  choices: ['Pre-production','Production'], description: 'Выберите контур для сборки')
        string(name: 'REPO_URL',   defaultValue: )')
    }

    agent any
    
    environment {
        db_ched2_predprod = credentials('db_ched2_predprod')
        db_ched2_prod = credentials('db_ched2_prod')  
        db_name = 'ched2_migration_journal'
        db_user = 'ched2_migration_journal'
        db_server = "${env_server[params.Environments]['server_db']}"
        db_pass = "${env_server[params.Environments]['db_pwd']}"

        TELEGRAM_TOKEN=""
        TELEGRAM_CHAT_ID=""
        TELEGRAM_OK = '✳️'
        TELEGRAM_NOOK = '☀️'
        TELEGRAM_FOUND = '❇️'
        TELEGRAM_NOTFOUND = ''
        TELEGRAM_BSTART='⚠️'
        TELEGRAM_SUCCESS = '✅'
        TELEGRAM_ABORTED = '⏹'
        TELEGRAM_FAILURE = '❌'  
    }

    stages {
        stage("start deploy") {
            steps {
                script {
                    if (env.gitlabSourceRepoHttpUrl){ //запуск через вебхук
                        env.REPO_URL = env.gitlabSourceRepoHttpUrl
                        user = "Автор комита: <b>${env.gitlabUserName.tokenize(' ')[0]}</b>"
                    } else { //запуск с параметрами через дженкинс
                        wrap([$class: 'BuildUser']) {
                        user = "Пользователь: <b>${env.BUILD_USER}</b>"
                    }
                    //    user = "anonymous"
                    }
                    try {
                        env.REPO_DIR = ((env.REPO_URL.toUpperCase() =~ /([^\/]*)\.GIT$/)[0][1]).toString()
                        currentBuild.displayName = "#${env.BUILD_NUMBER} " + "(${env.Environments}) " + "$REPO_DIR" 
                        env.REPO_FLYWAY_SCHEMA_TABLE = 'flyway_' + env.REPO_DIR.replace('.', '_').replace('-', '_').replace(' ', '_').toLowerCase()
                    }  catch (err) {
                        env.REPO_BUILD_FAILED_MSG = "${TELEGRAM_FAILURE} ошибка парсинга ссылки на репозиторий ${REPO_URL}:\n  ${err.getMessage()}"
                        error err.getMessage()
                    }  finally {
                        sendTgMsg("""${env.TELEGRAM_BSTART} Запущена сборка $env.BUILD_TAG <b>$user</b> в контуре ${Environments} с номером сборки <a href=\'${env.BUILD_URL}\'>${env.BUILD_NUMBER}</a>\n""" +
                        """Репозиторий: <a href=\'${env.REPO_URL}\'>${env.REPO_DIR}</a>\n""" +
                        """<a href=\'${BUILD_URL}console\'>Лог сборки</a>\n""")
                        sh 'printenv | sort | grep -E "(REPO|gitlab|TELEGRAM)"'
                    }
                }
            }
        }

        stage('clone gitlab') {
            steps {
                    dir(env.REPO_DIR) {
                    git branch: 'release', credentialsId: "gitlab-cred", url: env.REPO_URL
                    sh ''' git branch -a '''
                    sh ''' git checkout release '''
                }
            }
        }    

        stage ('db migrate') {
            when {
                expression { fileExists("${env.REPO_DIR}/sql/ched2_migration_journal_db") }
            }
            steps {
                
                script {
                    try {
                        flywayMigrateDb()
                    }  catch (err) {
                        sh 'pwd'
                       sendTgMsg("${env.TELEGRAM_FAILURE} Ошибка при миграции БД. <a href=\'${BUILD_URL}console\'>Лог сборки</a>")
                        throw err 
                    } 
                    
                }
            }
        }
    }
}
